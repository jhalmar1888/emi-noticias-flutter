
import 'calendario.dart';
import 'equipos.dart';
import 'grupos.dart';
import 'noticias.dart';
import 'package:flutter/material.dart';

class PaginaMenu extends StatefulWidget{

  @override
  _PaginaMenu createState() => new _PaginaMenu();
}

class _PaginaMenu extends State<PaginaMenu> {

  @override

  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(title: Text("MENU OPCIONES"),
      ),
    //  body: Center(child: Text("ESTAS EN EL MENU DE OPCIONES"),
    //  ),
      body: Container(
        padding: EdgeInsets.only(top:100),
        decoration: BoxDecoration(
          image: DecorationImage(image: AssetImage("assets/img/Bienvenidos-EMI-v3.png"),
            fit: BoxFit.contain,
            alignment: Alignment.topCenter
          )
        ),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                Column(
                  children: <Widget>[
                    Padding(
                      child: RaisedButton(
                        onPressed: (){

                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext) => PaginaNoticias())
                          );

                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30))
                        ),
                        child: SizedBox(
                          width: 100,
                          height: 120,
                          child: Center(
                            child: Text("Menu Noticias"),
                          ),
                        ),
                      ),
                      padding: const EdgeInsets.all(10.0),
                    )
                  ],
                ),

                Column(
                  children: <Widget>[
                    Padding(
                      child: RaisedButton(
                        onPressed: (){

                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext) => PaginaEquipos())
                          );

                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30))
                        ),
                        child: SizedBox(
                          width: 100,
                          height: 120,
                          child: Center(
                            child: Text("Menu Equipos"),
                          ),
                        ),
                      ),
                      padding: const EdgeInsets.all(10.0),
                    )
                  ],
                ),


              ],
            ),

          Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Column(
              children: <Widget>[
                Padding(
                  child: RaisedButton(
                    onPressed: (){

                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext) => PaginaNoticias())
                      );

                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30))
                    ),
                    child: SizedBox(
                      width: 100,
                      height: 120,
                      child: Center(
                        child: Text("Menu Noticias"),
                      ),
                    ),
                  ),
                  padding: const EdgeInsets.all(10.0),
                )
              ],
            ),

            Column(
              children: <Widget>[
                Padding(
                  child: RaisedButton(
                    onPressed: (){

                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext) => PaginaEquipos())
                      );

                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30))
                    ),
                    child: SizedBox(
                      width: 100,
                      height: 120,
                      child: Center(
                        child: Text("Menu Equipos"),
                      ),
                    ),
                  ),
                  padding: const EdgeInsets.all(10.0),
                )
              ],
            ),


          ],
        ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                Column(
                  children: <Widget>[
                    Padding(
                      child: RaisedButton(
                        onPressed: (){

                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext) => PaginaNoticias())
                          );

                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30))
                        ),
                        child: SizedBox(
                          width: 100,
                          height: 120,
                          child: Center(
                            child: Text("Menu Noticias"),
                          ),
                        ),
                      ),
                      padding: const EdgeInsets.all(10.0),
                    )
                  ],
                ),

                Column(
                  children: <Widget>[
                    Padding(
                      child: RaisedButton(
                        onPressed: (){

                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext) => PaginaEquipos())
                          );

                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30))
                        ),
                        child: SizedBox(
                          width: 100,
                          height: 120,
                          child: Center(
                            child: Text("Menu Equipos"),
                          ),
                        ),
                      ),
                      padding: const EdgeInsets.all(10.0),
                    )
                  ],
                ),


              ],
            )


          ],
        ),
      ),
    );
  }
}